import random, time, hmac, hashlib, base64
from wobble import slog

def gen_number(digits, prefix=''):

    number = str(random.randint(1,9))

    for x in range(1, digits-len(prefix)):
        n = str(random.randint(0,9))
        number += n
    return prefix+number



def hmac_headers(username, client_hash, key,  method, path):
    nonce = random.randint(0,99999)

    datestr = time.strftime('%a, %d %b %Y %H:%M:%S %Z')

    hashdata = method + '+' + path + '+' + datestr + '+' + str(nonce)
    hash = hmac.new(key, hashdata, hashlib.sha256).hexdigest()
    digest = base64.b64encode(hash)

    headers = {
        'Content-Type': 'application/json',
        'Date' : datestr,
        'Authorization' : 'hmac ' + username + ':' + str(nonce) + ':' + digest
        }
    return headers