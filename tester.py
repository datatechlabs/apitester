#!/usr/bin/python

import base64
import getopt
import hashlib
import hmac
import httplib
import random
import sys
import time
import csv
import threading

from wobble import slog, conf, tools

from testutils import utils
from testutils.tests import test_call_t

config_file = './conf/tester.conf.default'


# parse command-line options
def main():
	global config_file

	try:
		(opts, args) = getopt.getopt(sys.argv[1:], "hc:D")
	except getopt.GetoptError:
		usage()
		sys.exit(1)
	debug_mode = None
	for (opt, val) in opts:
		if opt == "-h":
			usage()
			sys.exit()
		if opt == "-c":
			config_file = val
		if opt == "-D":
			debug_mode = True


	if len(args) != 3:
		usage()
		sys.exit()

	endpoint = args[0]
	method = args[1]
	datafile = args[2]
	slog.info("Endpoint", endpoint, "method", method, "datafile", datafile)

	# read configuration file
	conf.read(config_file)
	# slog.info("configuration", conf.get_all())

	with open(datafile, 'rb') as f:
		reader = csv.reader(f)
		datalist = list(reader)

	# rearrange into list of dicts
	headers = datalist[0]
	rows = datalist[1:]
	attrs = []
	for row in rows:
		arow = {}
		i = 0
		for header in headers:
			arow[header] = row[i]
			i = i+1
		attrs.append(arow)

	packet_rate = conf.get_float('CLIENT', 'packet_rate')

	if packet_rate == 0:
		slog.warn("Packet rate is zero, exiting")
		sys.exit(0)

	# set logging stream
	daemon = conf.get_bool("MAIN", "daemon")

	if daemon:
		log_file = conf.get_str("MAIN", "log_file")
		slog.set_stream(log_file)

	# set logging levels
	if debug_mode:
		log_levels = ['error', 'warning', 'info', 'verbose']
	else:
		log_levels = ['error', 'warning']
	slog.output_tags(log_levels)

	# daemonize process

	if daemon:
		tools.daemonize()

	sttime = time.time()
	pktid = 0
	intid = 0
	inttime = 0
	calls = []
	datarowid = 0

	runonce = conf.get_bool('DEBUG', 'runonce', False)
	conf.set_value('STATS', 'start_time', sttime)
	max_packets = conf.get_int('MAIN', 'max_packets')
	update_interval = conf.get_int('MAIN', 'update_interval', 10)

	slog.info("Test started")

	while True:
		if runonce == True and pktid > 1:
			slog.warn("Stopping because run once is set")
			return
		tstamp = time.time() - sttime
		if pktid == 0:
			speed = 0
		else:
			speed = pktid / tstamp

		if speed > packet_rate:
			continue

		print(speed)

		if pktid >= max_packets and max_packets != 0:
			slog.info("maximum number of packets sent, stopping")
			return
		if intid >= update_interval:
			intspeed = intid / (tstamp - inttime)
			inttime = tstamp
			intid = 0
			slog.warn(pktid, "of", max_packets, "done. Speed : ", intspeed, "requests/sec")
		slog.verb("preparing packet #", pktid, "time", tstamp)
		intid += 1


		if speed <= packet_rate:
			tf = test_call_t(endpoint, method, attrs[datarowid], pktid)
			tf.start()
			calls.append(tf.result)
			pktid += 1
			datarowid += 1
			if datarowid >= len(rows):
				datarowid = 0



def print_stats():

	send_end = time.time()
	running = True
	while running:
		count = threading.activeCount()
		if count == 1:
			running = False


	start_time = conf.get_float('STATS', 'start_time')

	call_tot = conf.get_float('STATS', 'call_tot', 0)
	call_num = conf.get_float('STATS', 'call_num', 0)
	call_min = conf.get_float('STATS', 'call_min', 0)
	call_max = conf.get_float('STATS', 'call_max', 0)

	max_packets = conf.get_int('MAIN', 'max_packets')

	send_time = send_end - start_time
	rec_time = time.time() - start_time

	send_speed = ( max_packets ) / send_time
	rec_speed = ( max_packets ) / rec_time


	msg = '\n===============================================\n'
	msg += "Test completed.\n"
	msg += "Send speed is %s req/s completed in %s seconds\n" % (send_speed, send_time)
	msg += "Receive speed is %s req/s completed in %s seconds\n\n" % (rec_speed, rec_time)

	msg += "Requests:\n\n"
	msg += "Sent: %i Success: %i Failed %i\n" % (max_packets, call_num, max_packets-call_num)
	if call_num > 0:
		msg += "Avg response: %s ms\n" % (call_tot / call_num * 1000)
		msg += "Min response: %s ms\n" % (call_min * 1000)
		msg += "Max response: %s ms\n" % (call_max * 1000)

	msg += "\n"
	msg += '\n===============================================\n'

	print(msg)


def usage():
	print "./tester.py  [ -c configfile] [-h] [-d] endpoint method datafile"
	print " -c configfile - configuration file, default: ./conf/tester.conf"
	print " -D debug mode"
	print " -h this help"
	print "endpoint: one, of:"
	print "\t/sms/send"
	print "method: POST or GET"
	print "datafile: file containing payload names and data"


main()

print_stats()