from threading import Thread
from wobble import slog, conf, tools
import utils
import urllib
import urllib2
import time
import json

class test_call_t(Thread):
	def __init__(self, endpoint, method, attrs, pktid):
		self.result = None
		self.endpoint = endpoint
		self.method = method
		self.attrs = attrs
		self.pktid = pktid
		super(test_call_t, self).__init__()

	def run(self):
		self.result = test_call(self.endpoint, self.method, self.attrs, self.pktid)


def test_call(endpoint, method, attrs, pktid):
	api_base = conf.get_str('CLIENT', 'api_base')

	if method not in ["POST", "GET"]:
		slog.err("Method unknown:", method)
		return

	auth_method = conf.get_str('CLIENT', 'auth_method')
	auth_token = conf.get_str('CLIENT', 'auth_token')

	if auth_method != "token":
		slog.err("Auth method not implemented:", auth_method)
		return

	mod_attrs = {}
	for attr in attrs:
		v = attrs[attr]
		v = v.replace('%{number}', str(pktid))
		mod_attrs[attr] = v

	data = json.dumps(mod_attrs)
	tnow = time.time()

	url = api_base + endpoint
	slog.verb("sending to ", url)
	slog.info('==========')
	slog.info(data)
	slog.info('==========')

	req = urllib2.Request(url=url, data=data)
	req.add_header("Authorization", "token " + auth_token)

	try:
		resp = urllib2.urlopen(req)
	except urllib2.HTTPError as e:
		print("Error = " + str(e.code))
		print("Response = " + str(e.read()))
		return

	# update stats
	tdiff = time.time() - tnow
	tt = conf.get_float('STATS', 'call_tot', 0)
	conf.set_value('STATS', 'call_tot', tt + tdiff)

	nn = conf.get_float('STATS', 'call_num', 0)
	conf.set_value('STATS', 'call_num', nn + 1)

	nmin = conf.get_float('STATS', 'call_min', 1000000)
	if tdiff < nmin:
		conf.set_value('STATS', 'call_min', tdiff)

	nmax = conf.get_float('STATS', 'call_max', 0)
	if tdiff > nmax:
		conf.set_value('STATS', 'call_max', tdiff)

	resp_data = resp.read()
	slog.verb("RESPONSE: ", resp_data, " TIME ", tdiff)
	try:
		a = json.loads(resp_data)
	except:
		slog.info("=====ERROR=======")
		slog.info("RESPONSE CODE:", response.getcode())
		slog.info("HEADERS: ", response.info().items())
		slog.info("BODY: ", resp_data)
		slog.info("=====ERROR=======")
		return None
	request_id = a['request_id']
	return request_id